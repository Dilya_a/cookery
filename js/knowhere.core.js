;(function($){

	'use strict';

	$.knowhere = {

		DOMReady: function(){

			// full width section init
			if($('.kw-section.kw-fw').length) this.templateHelpers.fwSection.init();
			if($('.kw-section.kw-fw-bg').length) this.templateHelpers.fwSectionBg.init();

			if($('.kw-breadcrumb-container.kw-type-2, .kw-breadcrumb-container.kw-type-3').length) this.templateHelpers.breadcrumb.init();

			// dynamically set background image 
			if($('[data-bg]').length) this.templateHelpers.bgImage();

			if($('.owl-carousel').length) this.templateHelpers.owlHelper.init();
			
			if($('.kw-shopping-cart-full').length) this.modules.removeProduct();

			// back to top button init
			this.modules.backToTop.init({
				easing: 'easeOutQuint',
				speed: 550,
				cssPrefix: 'kw-'
			});

			this.modules.modalWindows.init();
			this.modules.dropdown.init({
				'cssPrefix': 'kw-',
				'speed': 500,
				'animation-in': 'bounceIn',
				'animation-out': 'bounceOut'
			});

			// close btn init
			if($('.kw-close').length) this.modules.closeBtn();

			if($('.kw-gmap').length) this.modules.googleMaps();

			// initialization of the WP Job manager plugin
			if($('.job_listings').length) this.modules.JobManager.init();

			if($('.fieldset-job_hours').length) this.events.hidedFieldset();

		},

		OuterResourcesLoaded: function(){

			// sticky section init
			if($('.kw-sticky').length) this.modules.stickySection.init();
			if($('.kw-isotope').length) this.modules.isotope.init();

			var self = this;
			self.templateHelpers.productRating();

			window.productRating = self.templateHelpers.productRating;

			// init animation for counters
			if($('.kw-counters-holder').length) this.modules.animatedCounters.init();

			// init animation for progress bars
			if($('.kw-progress-bars-holder').length) this.modules.animatedProgressBars.init({
				speed: 500,
				easing: 'easeOutQuint'
			});

		},

		jQueryExtend: function(){

			$.fn.extend({

				kwImagesLoaded : function () {

				    var $imgs = this.find('img[src!=""]');

				    if (!$imgs.length) {return $.Deferred().resolve().promise();}

				    var dfds = [];

				    $imgs.each(function(){
				        var dfd = $.Deferred();
				        dfds.push(dfd);
				        var img = new Image();
				        img.onload = function(){dfd.resolve();};
				        img.onerror = function(){dfd.resolve();};
				        img.src = this.src;
				    });

				    return $.when.apply($,dfds);

				}

			});

		},

		modules: {

			/**
			 * Generates back to top button
			 **/
			backToTop: {

				init: function(config){
					
					var self = this;

					if(config) this.config = $.extend(this.config, config);

					this.btn = $('<button></button>', {
						class: self.config.cssPrefix+'back-to-top animated stealthy',
						html: '<span class="lnr lnr-chevron-up"></span>'
					});

					this.bindEvents();

					$('body').append(this.btn);

				},

				config: {
					breakpoint: 700,
					showClass: 'zoomIn',
					hideClass: 'zoomOut',
					easing: 'linear',
					speed: 500,
					cssPrefix: ''
				},

				bindEvents: function(){

					var page = $('html, body'),
						self = this;

					this.btn.on('click', function(e){

						page.stop().animate({

							scrollTop: 0

						}, {
							easing: self.config.easing,
							duration: self.config.speed
						});

					});

					this.btn.on($.knowhere.ANIMATIONEND, function(e){

						e.preventDefault();
						
						var $this = $(this);

						if($this.hasClass(self.config.hideClass)){

							$this
								.addClass('stealthy')
								.removeClass(self.config.hideClass + " " + self.config.cssPrefix + "inview");

						}

					});

					$(window).on('scroll.backtotop', { self: this}, this.toggleBtn);

				},

				toggleBtn: function(e){

					var $this = $(this),
						self = e.data.self;

					if($this.scrollTop() > self.config.breakpoint && !self.btn.hasClass(self.config.cssPrefix + 'inview')){

						self.btn
								.addClass(self.config.cssPrefix + 'inview')
								.removeClass('stealthy');

						if($.knowhere.ANIMATIONSUPPORTED){
							self.btn.addClass(self.config.showClass);
						}

					}
					else if($this.scrollTop() < self.config.breakpoint && self.btn.hasClass(self.config.cssPrefix + 'inview')){

						self.btn.removeClass(self.config.cssPrefix + 'inview');

						if(!$.knowhere.ANIMATIONSUPPORTED){
							self.btn.addClass('stealthy');
						}
						else{
							self.btn.removeClass(self.config.showClass)
									.addClass(self.config.hideClass);
						}

					}

				}

			},

			/**
			 * Describes the behavior of drop-down lists.
			 */
			dropdown: {

				config: {
					'cssPrefix': '',
					'speed': 1000,
					'animation-in': 'fadeInUp',
					'animation-out': 'fadeOutDown'
				},

				init: function(options){

					var self = this;

					if(options) $.extend(this.config, options);

					// Auxiliary properties

					Object.defineProperties(this, {

						invoker: {

							get: function(){

								return '.' + this.config.cssPrefix + 'dropdown-invoker';

							}

						},

						dropdown: {

							get: function(){

								return '.' + this.config.cssPrefix + 'dropdown-list';

							}

						},

						container: {

							get: function(){

								return '.' + this.config.cssPrefix + 'dropdown';

							}

						},

						activeClass: {

							get: function(){

								return this.config.cssPrefix + 'opened';

							}

						}

					});

					$('body').on('click.dropdown', this.invoker, { self: this }, this.handler);
					$(document).on('click.dropdown', function(e){

						e.stopPropagation();
						if(!$(e.target).closest(self.container).length) self.close($('body').find(self.dropdown));

					});

				},

				handler: function(e){

					var $this = $(this),
						self = e.data.self,
						dropdown = $this.siblings(self.dropdown),
						container = $this.closest(self.container);

					if(dropdown.length){

						if(!dropdown.data('initialized')) self.initDropdown(dropdown);

						if(!container.hasClass(self.activeClass)){

							container.addClass(self.activeClass);
							dropdown.addClass(self.config['animation-in']);

						}
						else{

							if(!dropdown.data('timeOutId')){

								self.close(dropdown);

							}


						}

					}

					e.stopPropagation();
					e.preventDefault();

				},

				close: function(dropdown){

					var self = this,
						container = dropdown.closest(this.container);

					dropdown
						.removeClass(self.config['animation-in'])
						.addClass(self.config['animation-out'])
						.data('timeOutId', setTimeout(function(){

							container.removeClass(self.activeClass);
							dropdown
								.removeClass(self.config['animation-out'])
								.data('timeOutId', false);

						}, self.config.speed));

				},

				initDropdown: function(dropdown){

					dropdown
						.addClass('animated')
						.attr('style', 'animation-duration: ' + this.config['speed'] + 'ms')
						.data('initialized', true);

				}

			},

			/**
			 * Describes the loading of the modal windows.
			 */
			modalWindows: {

				init: function(){

					var $body = $('body');

					$body.on('click.arcticmodal', '[data-modal]', function(e){

						var $this = $(this),
							path = $this.data('modal');

						if(path){

							$.arcticmodal({
								type: 'ajax',
								url: path,
								speed: 350,
								ajax: {
							        type: 'POST',
							        cache: false
							    },
								afterClose: function(){

									setTimeout(function(){

										if(!$body.find('.arcticmodal-container').length){

											$body.css({
												'overflow-x': 'hidden',
												'overflow-y': 'visible'
											});

										}

									}, 0);

								}
							});

						}

						e.preventDefault();
						e.stopPropagation();

					});

				}

			},

			/**
			 * Initialize global close event
			 * @return Object Core;
			 **/
			closeBtn: function(){

				$('body').on('click.globalclose', '.kw-close:not(.kw-shopping-cart-full .kw-close)', function(e){

					e.preventDefault();

					$(this).parent().stop().animate({
						opacity: 0
					}, function(){

						$(this).stop().slideUp(function(){

							$(this).remove();

						});

					});

				});

				return this;

			},

			removeProduct: function(collection){

				var c = collection ? collection : $('.kw-shopping-cart-full');

				c.on('click.removeProduct', '.kw-close', function(e){

					e.preventDefault();

					$(this).closest('tr').stop().fadeOut(function(){
						$(this).remove();
					});

				});

			},

			/**
			 * Sticky header section
			 **/
			stickySection: {

				STICKYPADDING: 10,
				MAXSTICKYHEIGHT: 90,

				init: function(){

					this.body = $('body');
					this.sticky = $('#header').find('.kw-sticky');

					if(!this.sticky.length) return;

					this.bindEvents();
					this.updateDocumentState();

				},

				updateDocumentState: function(){
					
					var self = this;

					if(self.resizeTimeoutId) clearTimeout(self.resizeTimeoutId);

					self.resizeTimeoutId = setTimeout(function(){

						self.reset();

						self.sticky.removeAttr('style');

						if($(window).width() < 768) return;

						self.stickyHeight = self.sticky.outerHeight();

						if(self.stickyHeight > self.MAXSTICKYHEIGHT){

							self.needScale = true;

							self.defPaddingTop = parseInt(self.sticky.css('padding-top'), 10);
							self.defPaddingBottom = parseInt(self.sticky.css('padding-bottom'), 10);

							self.stickyOffset = self.sticky.offset().top + self.defPaddingTop - self.STICKYPADDING;

						}
						else{

							self.needScale = false;
							self.stickyOffset = self.sticky.offset().top;

						}					

						$(window).trigger('scroll.sticky');

					}, 120);

				},

				reset: function(){

					var $w = $(window);

					this.sticky.removeClass('kw-sticked');

					this.freeSpace();

					if($w.width() < 768 && this.hasEvents){

						var spacer = this.sticky.siblings('.kw-sticky-spacer');
						if(spacer.length) spacer.remove();

						$w.off('scroll.sticky');
						this.hasEvents = false;

						return;

					}
					else if($w.width() >= 768 && !this.hasEvents){

						$w.on('scroll.sticky', {self: this}, this.scrollHandler);
						this.hasEvents = true;

					}

				},

				bindEvents: function(){

					var $w = $(window),
						self = this;

					$w.on('scroll.sticky', {self: this}, this.scrollHandler);
					$w.on('resize.sticky', function(){

						self.updateDocumentState();

					});
					self.hasEvents = true;

				},

				scrollHandler: function(e){

					var $w = $(this),
						self = e.data.self;

					if($w.scrollTop() > self.stickyOffset && !self.sticky.hasClass('kw-sticked')){

						self.sticky.addClass('kw-sticked');

						if(self.needScale){

							self.sticky.css({
								'padding-top': self.STICKYPADDING,
								'padding-bottom': self.STICKYPADDING
							});

						}

						self.fillSpace();

					}
					else if($w.scrollTop() <= self.stickyOffset && self.sticky.hasClass('kw-sticked')){

						self.sticky.removeClass('kw-sticked');

						if(self.needScale){
						
							self.sticky.css({
								'padding-top': self.defPaddingTop,
								'padding-bottom': self.defPaddingBottom
							});

						}

						self.freeSpace();

					}

				},

				fillSpace: function(){

					var self = this,
						parent = self.sticky.parent(),
						spacer = parent.children('.kw-sticky-spacer');

					if(spacer.length){
						spacer.show().css('height', self.stickyHeight);
						return false;
					}
					else{

						spacer = $('<div></div>', {
							class: 'kw-sticky-spacer',
							style: 'height:' + self.stickyHeight + 'px'
						});

						parent.append(spacer);

					}

				},

				freeSpace: function(){

					var self = this,
						parent = self.sticky.parent(),
						spacer = parent.children('.kw-sticky-spacer');

					if(spacer.length) spacer.hide();

				}

			},

			animatedProgressBars: {

				init: function(config){

					this.collection = $('.kw-pbar');
					if(!this.collection.length) return;

					this.holdersCollection = $('.kw-progress-bars-holder');
					this.w = $(window);

					this.preparePBars();

					$.extend(this.config, config);

					this.updateDocumentState();

					this.w.on('resize.animatedprogressbars', this.updateDocumentState.bind(this));

					this.w.on('scroll.animatedprogressbars', {self: this}, this.scrollHandler);

					this.w.trigger('scroll.animatedprogressbars');

				},

				config: {
					speed: $.fx.speed,
					easing: 'linear'
				},

				updateDocumentState: function(){

					this.breakpoint = this.w.height() / 1.4;

				},

				preparePBars: function(){

					this.collection.each(function(i, el){

						var $this = $(el),
							indicator = $this.children('.kw-pbar-inner'),
							value = $this.data('value');

						$this.add(indicator).data('r-value', value);
						$this.add(indicator).attr('data-value', 0);

						indicator.css('width', 0);

					});

				},

				scrollHandler: function(e){

					var self = e.data.self;

					self.holdersCollection.each(function(i, el){

						var holder = $(el);

						if(self.w.scrollTop() + self.breakpoint >= holder.offset().top && !holder.hasClass('kw-animated')){

							self.animateAllBarsIn(holder);
							holder.addClass('kw-animated');

							if(i === self.holdersCollection.length - 1) self.destroy();

						}

					});


				},

				animateAllBarsIn: function(holder){

					var self = this,
						pbarsCollection = holder.find('.kw-pbar');

					pbarsCollection.each(function(i, el){

						var pbar = $(el),
							indicator = pbar.children('.kw-pbar-inner'),
							value = pbar.data('r-value'),
							pbarWidth = pbar.outerWidth();

						indicator.stop().animate({
							width: value + '%'
						}, {
							duration: self.config.speed,
							easing: self.config.easing,
							step: function(now){
								pbar.add(indicator).attr('data-value', Math.round(now));
							}
						});

					});

				},

				destroy: function(){

					this.w.off('scroll.animatedprogressbars');

				}

			},	

			animatedCounters: {

				init: function(){

					this.collection = $('.kw-counter');
					if(!this.collection.length) return;

					this.w = $(window);

					this.prepareCounters();
					this.updateDocumentState();

					this.w.on('scroll.animatedcounter', {self: this}, this.scrollHandler);
					this.w.on('resize.animatedcounter', this.updateDocumentState.bind(this));

					this.w.trigger('scroll.animatedcounter');

				},

				updateDocumentState: function(){

					this.breakpoint = this.w.height() / 1.4;

				},

				prepareCounters: function(){

					this.collection.each(function(i, el){

						var $this = $(el),
							value = $this.data('value');

						$this.data('r-value', value);
						$this.attr('data-value', 0);

					});

				},

				scrollHandler: function(e){

					var self = e.data.self;

					self.collection.each(function(i, el){

						var counter = $(el);

						if(self.w.scrollTop() + self.breakpoint > counter.offset().top && !counter.hasClass('kw-animated')){

							counter.addClass('kw-animated');
							self.animateCounter(counter);

							if(i === self.collection.length - 1) self.destroy();

						}

					});

				},

				animateCounter: function(counter){

					var value = counter.data('r-value'),
						intId, currentValue = 0;

					intId = setInterval(function(){

						counter.attr('data-value', ++currentValue);

						if(currentValue === value) clearInterval(intId);

					}, 4);

				},

				destroy: function(){

					this.w.off('scroll.animatedcounter');
					this.w.off('resize.animatedcounter');

				}

			},

			isotope: {

				baseConfig: {
					itemSelector: '.kw-entry-wrap, .kw-advertising-wrap',
					percentPosition: true,
					transitionDuration: '0.5s'
				},

				init: function(){

					this.collection = $('.kw-isotope');
					if(!this.collection.length) return;

					if(window.navigator.userAgent.toLowerCase().indexOf('android') !== -1) this.collection.addClass('kw-android');

					$.extend(this.baseConfig, {
						isOriginLeft: !$.knowhere.ISRTL
					});

					this.run();

				},

				run: function(){

					var self = this;

					this.collection.each(function(i, el){

						var container = $(el),
							config = $.extend({
								layoutMode: container.data('masonry') ? 'masonry' : 'fitRows'
							}, self.baseConfig);

						if(container.data('filter')){

							self.initFilter(container);

						}

						if(container.data('load-more-element')){

							self.initLoadMore(container);

						}

						container.kwImagesLoaded().then(function(){

							container.isotope(config);

						});

					});

				},

				initFilter: function(isotope){

					var filterElement = $(isotope.data('filter'));

					if(!filterElement.length) return;

					filterElement.on('click.filter', '[data-filter]', function(e){

						e.preventDefault();

						var $this = $(this);

						$this
							.addClass('kw-active')
							.parent()
							.siblings()
							.children('[data-filter]')
							.removeClass('kw-active');

						isotope.isotope({filter: $this.data('filter')});

					});

				}

			},

			googleMaps: function(){

				if(!$('.kw-gmap').length) return;

				var mapsCollection = [],
					mapSettings = {

					map_options: {
						zoom: 15
					},

					locations: [
						{
							lat: 40.7707307,
							lon: -74.0210859,
							icon: 'images/marker.png',
							title: 'Main office'
						}
					],

					generate_controls: false,
					controls_on_map: false,
					view_all: false

				},

				mapStyles = {

					'satellite' : {
						map_options: {
							zoom: 18,
							mapTypeId: google.maps.MapTypeId.SATELLITE
						}
					},

					'grayscale' : {
						map_options:{
							zoom: 16
						},
						styles:{
							'grayscale': [{
						        "featureType": "water",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#e9e9e9"
						            },
						            {
						                "lightness": 17
						            }
						        ]
						    },
						    {
						        "featureType": "landscape",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#f5f5f5"
						            },
						            {
						                "lightness": 20
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 17
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 29
						            },
						            {
						                "weight": 0.2
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 18
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 16
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#f5f5f5"
						            },
						            {
						                "lightness": 21
						            }
						        ]
						    },
						    {
						        "featureType": "poi.park",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#dedede"
						            },
						            {
						                "lightness": 21
						            }
						        ]
						    },
						    {
						        "elementType": "labels.text.stroke",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 16
						            }
						        ]
						    },
						    {
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "saturation": 36
						            },
						            {
						                "color": "#333333"
						            },
						            {
						                "lightness": 40
						            }
						        ]
						    },
						    {
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "transit",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#f2f2f2"
						            },
						            {
						                "lightness": 19
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#fefefe"
						            },
						            {
						                "lightness": 20
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#fefefe"
						            },
						            {
						                "lightness": 17
						            },
						            {
						                "weight": 1.2
						            }
						        ]
						    }]
						}
					},

					'clean_cut' : {
						styles: {
							'clean_cut': [ {
						        "featureType": "road",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "lightness": 100
						            },
						            {
						                "visibility": "simplified"
						            }
						        ]
						    },
						    {
						        "featureType": "water",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#C6E2FF"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#C5E3BF"
						            }
						        ]
						    },
						    {
						        "featureType": "road",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#D1D1B8"
						            }
						        ]
						    }]
						}
					}

				};

				$('.kw-gmap').each(function(i, el){

					var map = $.extend(mapSettings, mapStyles[$(el).data('gmap-type')]);

					if($(el).data('locations')){

						map.locations = $(el).data('locations');

					}

					map.map_div = '#' + $(el).attr('id');

					mapsCollection.push(new Maplace(map).Load());

				});

				if(!mapsCollection.length) return;

				$(window).on('resize.map', function(){

				 	setTimeout(function(){

					 	mapsCollection.forEach(function(elem, index, arr){

					 		elem.Load();

					 	});

					 }, 100);

	            });

			},

			JobManager: {

				baseMapConfig: {
					map_options: {
						zoom: 15,
						scrollwheel: false
					},
					map_div: '#job_listings-gmap',
					generate_controls: false,
					controls_on_map: false,
					view_all: false
				},

				init: function(){

					this.map = $('#job_listings-gmap');
					this.controls = $('.search_jobs-actions-layout');
					this.loadMore = $('.job_listings-load-more');


					if(this.map.length) this.initMapModule();
					if(this.controls.length) this.initLayoutModule();
					if(this.loadMore.length) this.initLoadMoreModule();

				},

				initMapModule: function(){

					this.itemsCollection = $('.job_listings .job_listing.hentry');

					if(this.itemsCollection.length) {

						this.baseMapConfig.locations = [];
						this.fillLocations();

					}

					this.initMap($.extend(this.baseMapConfig, {

					}));

				},

				initMap: function(config){

					var self = this;

					this.mapPlace = new Maplace(config);
					this.mapPlace.Load();

					$(window).on('resize.job_listing-gmap', function(){

						if(self.timeOutId) clearTimeout(self.timeOutId);

						self.timeOutId = setTimeout(function(){

						 	self.mapPlace.Load();

						 }, 100);

					});

				},

				fillLocations: function(){

					var _self = this;

					this.itemsCollection.each(function(i, el){

						var $this = $(el);

						_self.baseMapConfig.locations.push({
							lat: $this.data('latitude'),
							lon: $this.data('longitude'),
							icon: $this.data('marker')
						});

					});

				},

				initLayoutModule: function(){

					this.controls.on('click', { self: this }, this.changeLayout);

				},

				changeLayout: function(e){

					var $this = $(this),
						container = $this.closest('.job_listings'),
						items = container.find('.job_listing.hentry');

					if(!items.length) return false;

					$this
						.addClass('active')
						.siblings('.search_jobs-actions-layout')
						.removeClass('active');

					container.removeClass('kw-grid-view kw-list-view').addClass('kw-' + $this.data('style') + '-view');


					e.preventDefault();
					return false;

				},

				initLoadMoreModule: function(){

					var self = this;

					this.loadMore.on('click.job_listings-load-more', function(e){

						var $this = $(this),
							container = $($(this).data('container')),
							maxItems = container.data('max-items');

						if(!container.length) return;

						$this.addClass('kw-loading');

						$.ajax({
							type: 'GET',
							url: 'json/demo_data.json',
							dataType: 'json',
							success: function(data){

								self.insertNewItems(container, data.items);
								$this.removeClass('kw-loading');

								if(container.find('.job_listing.hentry').length >= maxItems){

									$this.stop().slideUp({
										duration: 500,
										easing: 'easeOutQuint',
										complete: function(){

											$(this).add($(this).parent('[class*="align-"]')).remove();

										}
									});

								}

							},
							error: function(data){

								$this.removeClass('kw-loading');

							}
						});

						e.preventDefault();

					});

				},

				insertNewItems: function(container, items){

					var self = this,
						container = container.find('.job_listings'),
						ratingCollection = $();

					if(!container.length) return;

					$.each(items, function(i, el){

						var $item = $(Handlebars.compile(self.itemTemplate)(el)),
							rating = $item.find('.kw-rating');

						container.append($item);

						if(rating.length) ratingCollection = ratingCollection.add(rating);

						self.mapPlace.AddLocation({
							lat: el.latitude,
							lon: el.longitude,
							icon: el.marker,
							title: el.title
						},0, true);

					});

					$.knowhere.templateHelpers.productRating(ratingCollection);

				},

				itemTemplate: '<li data-latitude="{{latitude}}" data-longitude="{{longitude}}" data-marker="{{marker}}" class="job_listing job-type-{{type}} type-job_listing status-publish hentry">\
									<article class="job_listing-entry">\
										<div class="job_listing-thumb">\
											<a href="{{link}}"><img src="{{thumbnail}}" alt="{{title}}"></a>\
											<div class="job_listing-icons">\
												{{#icons}}\
												<span class="lnr lnr-{{icon}}"></span>\
												{{/icons}}\
											</div>\
										</div>\
										<a href="{{link}}" class="job_listing-clickbox"></a>\
										<div class="job_listing-info">\
											<div class="position">\
												<h3 class="job_listing-title"><a href="{{link}}">{{title}}</a></h3>\
											</div>\
											<div class="job_listing-entry-extra">\
												<div class="kw-rating" data-rating="{{rating}}"></div>\
											</div><!--/ .job_listing-entry-extra -->\
											<div class="location">{{location}}</div>\
											<div class="phone">{{phone}}</div>\
											<a href="#" class="pintpoint-link"><span class="lnr lnr-pushpin"></span> Pintpoint</a>\
										</div>\
									</article>\
								</li>'

			}

		},

		events: {

			hidedFieldset: function(){

				var $fieldset = $('.fieldset-job_hours');
				if(!$fieldset.length) return;
				$fieldset.find('.kw-hours-op-container').hide();

				$fieldset.on('click.hidedFieldset', '.kw-hours-op-title', function(e){
					$(this).closest('fieldset').toggleClass('open');
					$(this).siblings('.kw-hours-op-container').stop().slideToggle({
						duration: 500,
						easing: 'easeOutQuint'
					});
					e.preventDefault();
					e.stopPropagation();
				});
			}
		},

		templateHelpers: {

			/**
			 * Dynamically set background image
			 * @return jQuery collection;
			 **/
			bgImage: function(collection){

				collection = collection ? collection : $('[data-bg]');
				if(!collection.length) return;

				collection.each(function(i, el){

					var $this = $(el),
						imageSrc = $this.data('bg');

					if(imageSrc) $this.css('background-image', 'url('+imageSrc+')');

				});

				return collection;

			},

			fwSectionBg: {

				init: function(){

					var self = this;

					this.collection = $('.kw-section.kw-fw-bg');
					if(!this.collection.length) return;

					this.container = $('.container');
					this.w = $('[class*="-layout-type"]');

					this.render();

					$(window).on('resize.fwSection', function(){

						if(self.timer) clearTimeout(self.timer);

						self.timer = setTimeout(function(){

							self.render();

						}, 50);

					});

				},

				reset: function(){

					if(!this.collection) return;

					var bgElement = this.collection.find('.kw-bg-element');

					bgElement.css({
						'margin-left':'auto',
						'margin-right': 'auto'
					});

					this.render();

				},

				render: function(){

					var self = this;

					this.collection.each(function(i, el){

						var $this = $(el),
							out = Math.abs(self.w.offset().left - $this.offset().left) * -1,
							bgImage = $this.data('fw-bg'),
							bgElement = $this.find('.kw-bg-element');

						if(!bgElement.length){

							bgElement = $('<div></div>', {
								class: 'kw-bg-element'
							});

							if(bgImage) bgElement.css('background-image', 'url(' +bgImage+ ')');

							$this.prepend(bgElement);

						};

						bgElement.css({
							'margin-left': out,
							'margin-right': out
						});

					});

				}

			},

			fwSection: {

				init: function(){

					var self = this;

					this.collection = $('.kw-section.kw-fw');
					if(!this.collection.length) return;

					this.container = $('.container');
					this.w = $('[class*="-layout-type"]');

					this.render();

					$(window).on('resize.fwSection', function(){

						if(self.timer) clearTimeout(self.timer);

						self.timer = setTimeout(function(){

							self.reset();

						}, 50);

					});

				},

				reset: function(){

					if(!this.collection) return;

					this.collection.css({
						'margin-left': 0,
						'margin-right': 0
					});

					this.render();

				},

				render: function(){

					var self = this;

					this.collection.each(function(i, el){

						var $this = $(el),
							out = Math.abs(self.w.offset().left - $this.offset().left) * -1

						$this.css({
							'margin-left': out,
							'margin-right': out
						});

						var isotope = $this.find('.kw-isotope-container');
						if(isotope.length) isotope.isotope('layout');

					});

				}

			},

			/**
			 * Sets correct inner offsets in breadcrumbs (only for fixed header types)
			 * @return undefined;
			 **/
			breadcrumb: {

				init: function(){

					var header = $('#header'),
						breadcrumbs = $('.kw-breadcrumb-container.kw-type-2, .kw-breadcrumb-container.kw-type-3'),
						$w = $(window);

					function correctPosition(){

						if($w.width() < 768) return false;

						var hHeight = header.outerHeight();
						breadcrumbs.css({
							'border-top-width': hHeight,
							'margin-top': hHeight * -1
						});

					}

					correctPosition();
					$(window).on('resize.breadcrumbs', correctPosition);

				}

			},

			owlHelper: {

				baseConfig: {
					items: 1,
					loop: true,
					nav: true,
					navElement: "button",
					dots: false,
					navText: [],
					rtl: getComputedStyle(document.body).direction === 'rtl',
					autoplay: true,
					autoplayTimeout: 4000,
					autoplayHoverPause: true,
					smartSpeed: 350,
					autoplaySpeed: 350,
					navSpeed: 350,
					dotsSpeed: 350,
					animateIn: 'fadeInLeft',
					animateOut: 'fadeOutRight'
				},

				init: function(collection){

					collection = collection ? collection : $('.owl-carousel');
					if(!collection.length) return;

					collection.addClass('kw-loading');

					this.adaptive(collection);

				},

				adaptive: function(collection){

					var self = this;

					collection.kwImagesLoaded().then(function(){

						collection.each(function(i, el){

							var $this = $(el);

							$this.on('resized.owl.carousel', function(e){

								self.containerHeight($this);

							});

							$this.on('changed.owl.carousel', function(e){

								self.containerHeight($this);

							});

							self.containerHeight($this);
							$this.removeClass('kw-loading');

						});

					});

				},

				containerHeight: function(owl){

					setTimeout(function(){

						var max = 0,
							items = owl.find('.owl-item'),
							activeItems = items.filter('.active').children();

						items.children().css('height', 'auto');

						activeItems.each(function(i, el){

							var $this = $(el),
								height = $this.outerHeight();

							if(height > max) max = height;

						});

						owl.find('.owl-stage-outer').stop().animate({
							height: max
						}, {
							duration: 150,
							complete: function(){

								var isotopeContainer = owl.closest('.kw-isotope');
								if(isotopeContainer.length) isotopeContainer.isotope('layout');								

							}
						});

					}, 20);

				}

			},

			/**
				** product raring
				**/
				productRating : function(collection){

					var $ratings = collection ? collection : $('.kw-rating');

					$ratings.each(function(){

						$(this).append("<div class='kw-empty-state'><i class='kw-icon-star-empty'></i><i class='kw-icon-star-empty'></i><i class='kw-icon-star-empty'></i><i class='kw-icon-star-empty'></i><i class='kw-icon-star-empty'></i></div><div class='kw-fill-state'><i class='kw-icon-star'></i><i class='kw-icon-star'></i><i class='kw-icon-star'></i><i class='kw-icon-star'></i><i class='kw-icon-star'></i></div>");

						var $this = $(this),
							rating = $this.data("rating"),
							fillState = $this.children('.kw-fill-state'),
							w = $this.outerWidth();

						fillState.css('width', Math.floor(rating / 5 * w));

					});

				},

		}

	};

	$.knowhere.jQueryExtend();

	$(function(){

		$.knowhere.DOMReady();

	});

	$(window).on('load', function(){

		$.knowhere.OuterResourcesLoaded();

	});

})(jQuery);