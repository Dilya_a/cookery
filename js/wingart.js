// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var tabsBox    = $(".tabs"),
 	styler     = $(".styler"),
	navBtn     = $("#toggle_nav_btn"),
	dropezone  = $(".dropzone"),
	owl		   = $(".slider"),
	fancybox   = $(".fancybox"),
	navOverlay = $(".nav_overlay");

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}

if(styler.length){
	include("js/formstyler.js");
}

if(dropezone.length){
	include("js/dropzone.js");
}

if(owl.length){
	include("js/owl.carousel.js");
}

if(fancybox.length){
	include("js/jquery.fancybox.js");
}

	include("js/chosen.jquery.js");
	include("js/modernizr.js");
	include("js/autoresize.jquery.js");

// if($(".data-mh").length){
//   include("js/jquery.matchHeight-min.js");
// }
// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  // $(".resp_categ_btn, .category_btn, .wrap_category_menu").on("click ontouchstart", function(){
  // 	$(".wrap_category_menu").toggleClass("navTrue");
  // })

  $('.resp_categ_btn, .category_btn').on('click ontouchstart', function(event){
      var current = $(this),
          target  = $('.wrap_category_menu');

      if ($(target).length){
        current.toggleClass('navTrue');
        $(target).toggleClass('navTrue');

        $(document).on("click", function(event) {
            if ($(event.target).closest(current).length || $(event.target).closest($(target)).length) return;
            $('body').find($(target)).removeClass("navTrue");
            $('body').find(current).removeClass("navTrue");
            event.stopPropagation();
          })
      }
    });


    $("#search_btn2, #close").on('click ontouchstart', function(e){
  		var body = $('body').find('.header_search2_block');
  		
  		body.toggleClass('search_overlay');

    })


	$(".active_btn, .search_block_close").on("click ontouchstart", function(){
		$(".md_touch").find(".search_block").toggleClass("active");

	})


    $(".autoriz_box").on("click ontouchstart", function(){
  	    $(".md_touch").find(".autorization_block_drop_resp").toggleClass("active");
  		
    })

    /*---  show social for share  ---*/

	    $(".share_btn, .close_soc_btn").on("click ontouchstart", function(){
	    	var main = $("body").find(".main");

	    	main.toggleClass("share_overlay");
	    })

	/*---  show settings for creating  ---*/

	    $(".create_btn").on("click ontouchstart", function(){
	    	var main = $("body").find(".main");

	    	main.toggleClass("create_overlay");
	    })

  // $(".md_touch").find(".resp_categ_btn.d_none").on("click ontouchstart", function(){
  // 	$(".wrap_category_menu").toggleClass("active");
  // });
	
  	/* ------------------------------------------------
	AUTORESIZE TEXTAREA START
	------------------------------------------------ */
			 
		   $(function(){

			    $('textarea').autoResize();

			});


	/* ------------------------------------------------
	AUTORESIZE TEXTAREA END
	------------------------------------------------ */


	/* ------------------------------------------------
	ANCHOR START
	------------------------------------------------ */
			 
		    function goUp(){
				var windowHeight = $(window).height(),
					windowScroll = $(window).scrollTop();

				if(windowScroll>windowHeight/2){
				  $('.arrow_up').addClass('active');
				}

				else{
				  $('.arrow_up').removeClass('active');
				  $('.arrow_up').removeClass('click');
				}

		    }

		    goUp();
			$(window).on('scroll',goUp);

			$('.arrow_up').on('click ontouchstart',function () {

				if($.browser.safari){
					$(this).addClass("click")
					$('body').animate( { scrollTop: 0 }, 1100 );
				}
				else{
					$(this).addClass("click")
					$('html,body').animate( { scrollTop: 0}, 1100 );
				}
				return false;
				
			});

	/* ------------------------------------------------
	ANCHOR END
	------------------------------------------------ */



	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

			if (styler.length){
				styler.styler();
			}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */


	/* ------------------------------------------------
	OWL START
	------------------------------------------------ */

			function widthW(){

				var windowW  = $(window).width();

				if( windowW <= 768){

					if($('.slider').length){
						$('.slider').owlCarousel({
							singleItem : true,
							items : 1,
							smartSpeed:1000,
							nav: false,
							dot: true
						});
					}	
				}
				else {
					// $('.slider').data('owlCarousel').destroy();
				}
			}
			widthW();

			$(window).on('scroll',function(){
		      	widthW();
		    });

	/* ------------------------------------------------
	OWL END
	------------------------------------------------ */



	/* ------------------------------------------------
	FANCYBOX START
	------------------------------------------------ */
			 

	        if(fancybox.length){
				fancybox.fancybox();
			}

	/* ------------------------------------------------
	FANCYBOX END
	------------------------------------------------ */



	/* ------------------------------------------------
	CHOSEN START
	------------------------------------------------ */

			var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }

		    if($('.chosen-select').length){

			    var $this = $('.chosen-select'),
			    	inputFocus  = $('.search-field'),
			    	container   = $('.chosen-container');


		    	inputFocus.on('keypress', function(){

					container.addClass('focus');

		    	});


		    	$(document).on("click ontouchstart", function(event) {
		        	
		        	if ($(event.target).closest($this).length) return;
					
					container.removeClass('focus');         
		         
		        	event.stopPropagation();
		       	});

		    }
   	/* ------------------------------------------------
	CHOSEN END
	------------------------------------------------ */

	/* ------------------------------------------------
	DATA-BG START
	------------------------------------------------ */

		if($(".img_box").length){
		    $(".img_box").each(function(){
		      var current = $(this),
		          //linkAttr = current.attr("href");
				linkAttr = $(current).find('img').attr('src');
		        current.css({'background-image' : "url(" + linkAttr + ")"});
		        // console.dir($(this).find('img'));
			});
		}

	/* ------------------------------------------------
	DATA-BG END
	------------------------------------------------ */




	// if ($('.chosen-container').length > 0) {
 //      $('.chosen-container').on('touchstart', function(e){
 //        e.stopPropagation(); e.preventDefault();
 //        // Trigger the mousedown event.
 //        $(this).trigger('mousedown');
 //      });
 //    }


})